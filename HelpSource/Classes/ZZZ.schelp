TITLE:: ZZZ
summary:: Abstract superclass of all module specific Expert Sleepers classes
categories:: External Control>Eurorack
related:: Classes/ZZZDevice

DESCRIPTION:: Users will normally not directly create instances of ZZZ, but
only use it through its subclasses. It provides basic conversion methods, that
can be used to drive DC coupled hardware of the Eurorack standard.
Additionally it provides link::Classes/SynthDef##SynthDefs::, which will be
added during instantiation, that can be used for clock and gate signals.

CLASSMETHODS::

subsection:: Initialization

METHOD:: new
Initialize a new instance.

note::Do not call this method directly but instead derive a class from this one
and use this method in the child's classes initialization method (see
link::Classes/ZZZDevice:: for an example).::

ARGUMENT:: channels
An link::Classes/Array:: of link::Classes/Integer##Integers:: representing
output bus channels (within the range of
link::Classes/ServerOptions#-numOutputBusChannels::) on a
link::Classes/Server::.

ARGUMENT:: server
An instance of link::Classes/Server::.

returns:: An instance of link::Classes/ZZZ::.

METHOD:: initClass
Uses link::Classes/StartUp#*add:: to add link::Classes/SynthDef##SynthDefs:: 
used by child classes.

list::
## code::\ZZZGate::: A gate link::Classes/SynthDef:: utilizing link::Classes/DC::.
## code::\ZZZClock::: A clock link::Classes/SynthDef:: (24 beats per quarter
note) utilizing link::Classes/LFPulse::.
## code::\ZZZClockKorg::: A clock link::Classes/SynthDef:: (48 beats per quarter
note) utilizing link::Classes/LFPulse::.
::

subsection::Conversion

METHOD:: ampmidi
Calculates midi note for supplied amplitude.

ARGUMENT:: amplitude
Amplitude in the range from 0.0 to 1.0.

returns:: MIDI note for corresponding amplitude, nil if amplitude is not within
the allowed octave range.

METHOD:: noteCpsRangeMax
Cps for MIDI note C10.

returns:: Returns cps for MIDI note C10.

METHOD:: cvMaxADSR
Maximum voltage for ADSR on Eurorack modules.

returns:: Returns maximum voltage for ADSR.

METHOD:: noteCpsRangeMin
Cps for MIDI note C-2.

returns:: Returns cps for MIDI note C-2.

METHOD:: midiamp
Calculates amplitude for supplied MIDI note.

ARGUMENT:: midi
MIDI note.

returns:: Amplitude for corresponding midi note, nil if < 0 or > 127

METHOD:: cvMinADSR
Minimum voltage for ADSR on Eurorack modules.

returns:: Returns minimum voltage for ADSR.

METHOD:: cvMaxTriggerGateClock
Maximum trigger/gate/clock control voltage for Eurorack modules.

returns:: Returns maximum maximum trigger/gate/clock control voltage for Eurorack modules.

METHOD:: cpsvoltage
Calculates voltage for supplied cycles per second.

ARGUMENT:: cps
Cycles per seconds

returns:: Returns voltage for corresponding cycles per second.

METHOD:: noteVoltageRangeMin
Maximum voltage for a note on Eurorack modules.

returns:: Returns maximum voltage for notes.

METHOD:: amplitudevoltage
(describe method here)

ARGUMENT:: amplitude
(describe argument here)

returns:: (describe returnvalue here)

METHOD:: cvMaxLFO
(describe method here)

returns:: (describe returnvalue here)

METHOD:: noteVoltageRangeMax
(describe method here)

returns:: (describe returnvalue here)

METHOD:: ampcps
(describe method here)

ARGUMENT:: amplitude
(describe argument here)

returns:: (describe returnvalue here)

METHOD:: cvMinTriggerGateClock
(describe method here)

returns:: (describe returnvalue here)

METHOD:: voltageamplitude
(describe method here)

ARGUMENT:: voltage
(describe argument here)

returns:: (describe returnvalue here)

METHOD:: cpsamp
(describe method here)

ARGUMENT:: cps
(describe argument here)

returns:: (describe returnvalue here)

METHOD:: cvMinLFO
(describe method here)

returns:: (describe returnvalue here)

METHOD:: voltagecps
(describe method here)

ARGUMENT:: voltage
(describe argument here)

returns:: (describe returnvalue here)


INSTANCEMETHODS::

METHOD:: channels
Returns an link::Classes/Array:: of link::Classes/Integer##Integers:: 
representing output bus channels (within the range of
link::Classes/ServerOptions#-numOutputBusChannels::) on a
link::Classes/Server:: that was used in link::#*new:: to initialize an instance
of this class (or any of its subclasses).

returns:: An link::Classes/Array:: of link::Classes/Integer##Integers::.

METHOD:: server
Returns a link::Classes/Server:: instance that was used in link::#*new:: to
initialize an instance of this class (or any of its subclasses).

returns:: A link::Classes/Server:: instance.
