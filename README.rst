ZZZ
###

`SuperCollider <https://supercollider.github.io/>`_ classes to interface with
`Expert Sleepers <https://www.expert-sleepers.co.uk/>`_ devices.

Supported devices
-----------------

* `ES-3 <https://www.expert-sleepers.co.uk/es3.html>`_
* `ES-8 <https://www.expert-sleepers.co.uk/es8.html>`_

Features
--------

* **SynthDefs** for specific types of signals (clock and gate).
* Class methods to convert between MIDI notes, frequency, amplitude and
  corresponding output voltage.
* Instance methods to add, set and remove clock and gate signals on specific
  hardware outputs.
* Documentation (in *.schelp* format) for the **ZZZ**, **ZZZDevice** and
  **ZZZError** classes.

Installation
------------

The classes are an extension of the `SuperCollider
<https://supercollider.github.io/>`_ language (`sclang
<http://doc.sccode.org/>`_), therefore their installation corresponds to `how
Quarks are installed
<https://github.com/supercollider-quarks/quarks#installing-a-quark>`_.

Usage
-----

To create an object interfacing your hardware device, instantiate the
**ZZZDevice** class with an Array of Integers representing your eight ADAT
channels connected to the module and the server instance in use:

  .. code:: supercollider

    // RME Babyface example, when creating output bus channels for all hardware
    // outputs of the audio interface
    ~device = ZZZDevice.new(server: s, channels: Array.series(8, 4, 1));

  .. note::

    | The instantiation of *ZZZDevice* conveniently makes the outputs of the
      device available in a numbering range from 1-8 (as displayed on the
      hardware).
    | Generally, in SuperCollider channel numbering starts with 0, though!

To interface with your module on one of the eight outputs, you can make use of
the *SynthDefs* added by the *ZZZ* class:

  .. code:: supercollider

    // create a tempo (240bpm), which can be used by e.g. multiple clocks on outputs
    ~device.addTempo(slot: 1, tempo: (240/60));
    
    // the tempo can be changed manually
    ~device.setTempo(slot: 1, tempo: (180/60));
    
    // start clock on output 8 using the tempo at slot 1
    ~device.addClock(output: 8, slot: 1);
    
    // remove the clock
    ~device.removeClock(output: 8);

  .. code:: supercollider

    // start a gate  on output 7
    ~device.addGate(output: 7);

Both tempos and gates can be altered using control *Busses* exposed in
*.tempoBusses* and *.gateBusses* (respectively).

Background
----------

| Modular synthesizers are controllable through several ranges of voltage.
| However, `CV/ Gate <https://en.wikipedia.org/wiki/CV/gate>` as defined by
  `Doepfer <http://www.doepfer.de>`_ in the `A100 manual
  <http://www.doepfer.de/a100_man/a100t_e.htm>`_ might not be used coherently
  throughout all Eurorack modules.

| The *ZZZ* classes nonetheless try to adhere to the standard, while at the
  same time providing convenience functions to low-level conversion.
| By default the helper functions are able to convert frequencies in the `range
  of twelve octaves <https://en.wikipedia.org/wiki/C_(musical_note)>`_
  (between *C-2* and *C10*) to an output between -5V and 7V (adhering to the
  1V/octave scheme) - the MIDI note range being a subset of it.
| As the range is not fixed, but can be moved up and down (e.g. when using
  filters or oscillators), you might have to make some adjustments to your
  module, to get "in tune".

License
-------

This code is licensed under the terms of the **GPL-3.0-or-later** (see `LICENSE <LICENSE>`_).
