ZZZ{

  classvar <cvMinLFO = -2.5,
  < cvMaxLFO = 2.5,
  < cvMinADSR = 0.0,
  < cvMaxADSR = 8.0,
  < cvMinTriggerGateClock = 0.0,
  < cvMaxTriggerGateClock = 5.0,
  < noteVoltageRangeMin = -5.0,
  < noteVoltageRangeMax = 7.0,
  < noteCpsRangeMin = 4.088,
  < noteCpsRangeMax = 16744.036;
  var <channels, <server;

  *new{
    arg channels, server;
    ^super.newCopyArgs(channels, server);
  }

  *initClass{
    StartUp.add{
      SynthDef(\ZZZGate, {|bus = 0, in = 1.0, out = 0|
        OffsetOut.ar(
          out,
          DC.ar(in: in) * In.kr(bus)
        );
      }).add;
      SynthDef(\ZZZClock, { |out = 0, bus = 0, iphase = 0, width = 0.5, mul = 1.0, add = 0.0|
        OffsetOut.ar(out, LFPulse.ar(In.kr(bus), iphase, width, mul, add));
      }).add;
      SynthDef(\ZZZClockKorg, { |out = 0, bus = 0, iphase = 0, width = 0.5, mul = 1.0, add = 0.0|
        OffsetOut.ar(out, LFPulse.ar(In.kr(bus) * 2, iphase, width, mul, add));
      }).add;
      // TODO: revise mapping of ranges and how to shift reference point
      // NOTE: https://kentonuk.com/support/midi-to-cv/
      // NOTE: https://soundprogramming.net/file-formats/midi-note-frequencies/
      // NOTE: https://web.archive.org/web/20100409063540/http://www.users.globalnet.co.uk/~concuss/mcvbeg.htm
      SynthDef(\ZZZCV, { |out = 0, freq = 1, gate = 1, attackTime = 0.01, susLevel = 1.0, releaseTime = 0.3, midiAmpVO = 0.0, midiAmpHV = 0.0, freqAmpVO = 0.0, freqAmpHV = 0.0|
        // midi to V/O (C-1 - G9)
        var midiVO = LinLin.kr(in: freq, srclo: 0, srchi: 127, dstlo: -0.1, dsthi: 0.96666) * midiAmpVO,
        // midi to Hz/V (C-1 - C5)
        midiHV = LinExp.kr(in: freq, srclo: 0, srchi: 72, dstlo: 0.00625, dsthi: 0.8) * midiAmpHV,
        // frequency to V/O (C-1 - G9)
        freqVO = LinLin.kr(in: freq, srclo: 8.176, srchi: 12543.850, dstlo: -0.1, dsthi: 0.96666) * freqAmpVO,
        // frequency to Hz/V (C-1 - C5)
        freqHV = LinExp.kr(in: freq, srclo: 8.176, srchi: 523.251, dstlo: 0.00625, dsthi: 0.8) * freqAmpHV;
        OffsetOut.ar(
          out,
          // TODO: revise envelope to become more flexible
          DC.ar(in: midiVO + midiHV + freqVO + freqHV) * Linen.kr(gate: gate, attackTime: attackTime, susLevel: susLevel, releaseTime: releaseTime, doneAction: Done.freeSelf)
        );
      }, variants: (
        freqVO: [\freqAmpVO: 1.0],
        freqHV: [\freqAmpHV: 1.0],
        midiVO: [\midiAmpVO: 1.0],
        midiHV: [\midiAmpHV: 1.0],
      )).add;
      SynthDef('ZZZVCO', {|out = 0, freq = 440, iphase = 0.0, width = 0.0, mul = 1.0, add = 0.0, triAmp = 0.0, pulseAmp = 0.0, sawAmp = 0.0, sineAmp = 0.0|
        var triangle = LFTri.ar(freq: freq, iphase: iphase, mul: mul, add: add) * triAmp,
        pulse = LFPulse.ar(freq: freq, iphase: iphase, width: width, mul: mul, add: add) * pulseAmp,
        saw = LFSaw.ar(freq: freq, iphase: iphase, mul: mul, add: add) * sawAmp,
        sine = SinOsc.ar(freq, 0, 0.5) * sineAmp;
        OffsetOut.ar(
          out, triangle + pulse + saw + sine
        )
      }, variants: (
        triangle: [mul: 0.5, add: 0.0, triAmp: 1.0],
        pulse: [width: 0.5, mul: 1.0, add: -0.5, pulseAmp: 1.0],
        saw: [mul: 0.5, add: 0.0, sawAmp: 1.0],
        sine: [mul: 0.5, add: 0.0, sineAmp: 1.0],
        sawsine: [mul: 0.5, add: 0.0, sineAmp: 0.5, sawAmp: 0.5],
      )).add;
    }
  }

  /**
   * Calculates voltage for supplied amplitude.
   * Maximum: 10
   * Minimum: -10
   * @return voltage that corresponds to the amplitude supplied.
   */
  *amplitudevoltage{
    arg amplitude;
    var voltage = 0.0;
    if((amplitude == 0.0), {
      ^voltage;
    },{
      voltage = amplitude*10.0;
      if(voltage < -10.0, {
        ^(-10.0);
      });
      if(voltage > 10.0, {
        ^(10.0);
      });
      ^voltage;
    });
  }

  /**
   * Calculates amplitude for supplied voltage.
   * Maximum: 1.0
   * Minimum: -1.0
   * @return amplitude needed to achieve the voltage supplied.
   */
  *voltageamplitude{
    arg voltage;
    var amplitude = 0.0;
    if((voltage == 0.0), {
      ^amplitude;
    },{
      amplitude = voltage/10.0;
      if(amplitude < -1.0, {
        ^(-1.0);
      });
      if(amplitude > 1.0, {
        ^(1.0);
      });
      ^amplitude;
    });
  }

  /**
   * Calculates voltage for supplied cycles per second.
   * @return voltage for corresponding cycles per second
   */
  *cpsvoltage{
    arg cps;
    if(cps < noteCpsRangeMin, {
      cps = noteCpsRangeMin;
    });
    if(cps > noteCpsRangeMax, {
      cps = noteCpsRangeMax;
    });
    ^cps.explin(noteCpsRangeMin, noteCpsRangeMax, noteVoltageRangeMin, noteVoltageRangeMax);
  }

  /**
   * Calculates cycles per second for supplied voltage.
   * @return cycles per second for corresponding voltage
   */
  *voltagecps{
    arg voltage;
    if(voltage < noteVoltageRangeMin, {
      voltage = noteVoltageRangeMin;
    });
    if(voltage > noteVoltageRangeMax, {
      voltage = noteVoltageRangeMax;
    });
    ^voltage.linexp(noteVoltageRangeMin, noteVoltageRangeMax, noteCpsRangeMin, noteCpsRangeMax);
  }

  /**
   * Calculates amplitude for supplied frequency.
   * @return amplitude for corresponding frequency
   */
  *cpsamp{
    arg cps;
    ^this.voltageamplitude(this.cpsvoltage(cps));
  }

  /**
   * Calculates frequency for supplied amplitude.
   * @return frequency for corresponding amplitude
   */
  *ampcps{
    arg amplitude;
    ^this.voltagecps(this.amplitudevoltage(amplitude));
  }

  /**
   * Calculates amplitude for supplied midi note.
   * @return amplitude for corresponding midi note, nil if < 0 or > 127
   */
  *midiamp{
    arg midi;
    if((midi < 0 ) || (midi >127), {
      ^nil;
    },{
      ^this.voltageamplitude(this.cpsvoltage(midi.midicps));
    });
  }

  /**
   * Calculates midi note for supplied amplitude.
   * @return midi note for corresponding amplitude, nil if amplitude is not
   * within the allowed octave range
   */
  *ampmidi{
    arg amplitude;
    if((amplitude <= ((noteVoltageRangeMin+1)/10)) || (amplitude > (((noteVoltageRangeMax-1)+(1/12*7))/10)), {
      ^nil;
    },{
      ^this.voltagecps(this.amplitudevoltage(amplitude)).cpsmidi;
    });
  }


}
